package com.clovirsm.common;

import com.clovirsm.ISite;
 
import com.fliconz.fw.runtime.util.PropertyManager;

public class ClassHelper {
	public static ISite getSite( ) throws Exception
	{
		String className =   PropertyManager.getString("hv.before.class.V"  );
		if(className != null)
		{
			return (ISite)Class.forName(className).newInstance(  );
		}
		else
		{
			return null;
		}
	}
}
