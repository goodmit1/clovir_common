package com.clovirsm.common;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;

import com.clovirsm.security.NCSecurityConstant;
import com.clovirsm.service.workflow.DefaultNextApprover;
import com.clovirsm.service.workflow.INextApprover;
import com.fliconz.fm.admin.service.UserService;
import com.fliconz.fm.common.cache.MessageBundle;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.fliconz.fm.security.FMSecurityContextHelper;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fm.util.IMailSender;
import com.fliconz.fw.runtime.util.ListUtil;
import com.fliconz.fw.runtime.util.PropertyManager; 

public abstract class NCDefaultService extends DefaultService{

	@Autowired
	transient UserService userService;
	@Autowired
	IMailSender mailSender;
	org.apache.log4j.Logger maillog = LogManager.getLogger("mail");
	@Autowired INextApprover nextApprover;
	protected List getJSON2List(Map param, String key, String... copyFields) throws Exception
	{
		String jsonStr = (String) param.get(key);
		List<Map> list =  ListUtil.makeJson2List(jsonStr);
		if(copyFields != null)
		{
			for(String f:copyFields)
			{
				for(Map m : list)
				{
					m.put(f, param.get(f));
				}
			}
		}
		return list;
	}
	public List getAllTeamList(Object compId) throws Exception
	{
		
			Map param = new HashMap();
			param.put("COMP_ID", compId);
			return selectListByQueryKey("com.clovirsm.common.Component", "list_all_team", param);
			 
			
		
	}
	protected int insertOrUpdateMulti(String table, List<Map> params) throws Exception
	{
		int row = 0;
	
		for(Map param : params)
		{
			if("I".equals(param.get(IU_FILED)))
			{
				row += this.insertDBTable(table, param);
			}
			else if("U".equals(param.get(IU_FILED)))
			{
				int row1 = this.updateDBTable(table, param);
				if(row1==0)
				{
					row1 = this.insertDBTable(table, param);
				}
				row += row1;
			}
		}
		return row;
	}
	public boolean isOwner(Map selRow)
	{
		if(selRow.get("INS_ID") == null) return true;
		if(isAdmin()) return true;
		return UserVO.getUser().getUserId().toString().equals(selRow.get("INS_ID").toString());
	}
	
	public void sendMailToAdmin( String titleKey, String template,  Map paramMap) throws Exception
	{
		List<Object> adminIds = nextApprover.getMemberByRole(DefaultNextApprover.ROLE_POTAL_MANAGER, paramMap);
		paramMap.put("SendForAdmin", "Y");
		for(Object id : adminIds) {
			sendMail(id, titleKey, template, paramMap);
		}
		paramMap.remove("SendForAdmin");
	}
	public void sendMail(String toEmail, String title, String template,  String lang, Map paramMap)  {
		try
		{
			mailSender.send(toEmail ,  title,template, lang, paramMap);
			maillog.info((new Date()) + "----- mail send " + toEmail  + "  " +     title);
		}
		catch(Exception e)
		{
			maillog.error((new Date()) + "----- mail send error " +   toEmail  + "  " +  title + ":" + e.getMessage());
		}
	}
	public Map getUserInfo(Object userId) throws Exception{
		Map param = new HashMap();
		param.put("USER_ID",userId);
		return userService.selectInfo("FM_USER", param);
	}
	public void sendMail(Object userId, String titleKey, String template,  Map paramMap) throws Exception
	{
		 
		Map info = getUserInfo(userId);
		if(info  !=  null)
		{
			String lang = (String) info.get("LOCALE");
			if(TextHelper.isEmpty(lang))
			{
				lang = "ko";
			}
			String toEmail = (String) info.get("EMAIL");
			if(toEmail ==null || "".equals(toEmail)) return ;
			paramMap.put("USER_NAME", info.get("USER_NAME"));
			String title =  MsgUtil.getMsg( titleKey, null, lang);
			
			try
			{
				mailSender.send(toEmail ,  title,template, lang, paramMap);
				maillog.info((new Date()) + "----- mail send " + toEmail  + "  " +     title);
			}
			catch(Exception e)
			{
				maillog.error((new Date()) + "----- mail send error " +   toEmail  + "  " +  title + ":" + e.getMessage());
			}
		}
	}
	public void sendMailWithTitle(Object userId, String title, String template, String lang,  Map paramMap) throws Exception  {
		Map info = getUserInfo(userId);
		if(info  !=  null)
		{
			paramMap.put("USER_NAME", info.get("USER_NAME"));
			sendMail(new String[] { (String)info.get("EMAIL")}  ,   title,   template,    lang,   paramMap) ;
		}
		else {
			maillog.error((new Date()) + " user not found " + userId);
		}
	}
	public void sendMail(String[] toEmail, String title, String template,  String lang, Map paramMap)  {
		try
		{
			mailSender.send(toEmail ,  title,template, lang, paramMap);
			maillog.info((new Date()) + "----- mail send " + TextHelper.join(toEmail)  + "  " +     title);
		}
		catch(Exception e)
		{
			maillog.error((new Date()) + "----- mail send error " +   TextHelper.join(toEmail)  + "  " +  title + ":" + e.getMessage());
		}
	}

	public void chkDuplicate(String tableNm, Map selRow, String chkField) throws Exception
	{
		if(this.selectCountByQueryKey("selectDuplicateChk_" + tableNm, selRow)==0)
		{
			return ;
		}
		else
		{
			throw new NCException(NCException.DUPLICATE_NAME, TextHelper.nvl((String)selRow.get(chkField),""));
		}
	}
	 
	
	public boolean isAdmin()
	{
		List<String> roleName = UserVO.getUser().getRoleNames() ;
		String[] adminRole = PropertyManager.getStringArray("admin.role", new String[] {"ROLE_PORTAL_MANAGER","ROLE_APPROVER"});
		if(UserVO.getUser().isSuperAdmin()) return true;
		for(String r:adminRole)	{
			if(roleName.contains(r)) {
				return true;
			}
		}
		return false;
		
		
	}
	public boolean isGrpMember()
	{
		 
		List<String> roleName = UserVO.getUser().getRoleNames() ;
		return roleName.contains("ROLE_GRP_MEMBER") && !(roleName.contains("ROLE_GRP_MANAGER") || roleName.contains("ROLE_ORG_MANAGER")) ;
	}
	protected void addCommonParam(Map param){
		super.addCommonParam(param);
		try
		{
			param.put("_IS_ADMIN_", isAdmin() ? "Y" : "N");
			param.put("_USER_IP_", FMSecurityContextHelper.getFirstRemoteAddr());
			param.put("_ROLE_NM_", UserVO.getUser().getRoleNames().get(0));
			param.put("_TEAM_CD_", UserVO.getUser().getTeam().getTeamCd());
			param.put("_MILLIS_", System.currentTimeMillis());
			param.put("_CHILD_TEAM_CD_",UserVO.getUser().getChildrenTeamListMap().values());
			param.put("_SITE_CD_", PropertyManager.getString("site.code", ""));
		}
		catch(Exception ignore)
		{

		}
	}

	public boolean isChangeValue(Map selRow, String key){
		if(selRow.get("OLD_" + key)==null && selRow.get(key)==null) return true;
		if(selRow.get("OLD_" + key)==null || selRow.get(key)==null) return false;
		return !selRow.get(key).toString().equals(selRow.get("OLD_" + key).toString());
	}
}
