package com.clovirsm.common.chatbot;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.RestClient;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class ChatBotBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	int currentIdx = 0 ;
	int nextIdx = -1;
	List currentChoice;
	Map answerMap;
	RestClient client;
	public ChatBotBean(String sessionId) throws Exception {
		answerMap = new HashMap();
		client = new RestClient(ChatBotConfigFactory.getInstance().getBaseUrl());
		client.setMimeType("application/x-www-form-urlencoded");
		Map header = new HashMap();
		header.put("Cookie","JSESSIONID=" + sessionId);
		client.setHeader(header);
	}
	public int getCurrentIdx() {
		return currentIdx;
	}
	public void setCurrentIdx(int currentIdx) {
		this.currentIdx = currentIdx;
	}
	public List getCurrentChoice() {
		return currentChoice;
	}
	public void setCurrentChoice(List currentChoice) {
		this.currentChoice = currentChoice;
	}
	public Map getAnswer() {
		return answerMap;
	}
	public void setAnswer(Map answer) {
		this.answerMap = answer;
	}
	public void doAction(String action) throws  Exception {
		int pos = action.lastIndexOf(".");
		Object bean = SpringBeanUtil.getBean(action.substring(0, pos));
		bean.getClass().getMethod(action.substring(pos+1), Map.class).invoke(bean, answerMap);
	}
	public JSONObject answer(String answer) throws Exception {
		if(answer != null && !"".equals(answer)) {
			if("-1".equals(answer) && this.currentIdx>0) {
				setCurrentIdx(this.currentIdx -1);
				return  getQuestion();
			}
			if(currentChoice==null && "0".equals(answer)) {
				setCurrentIdx(0);
				return  getQuestion();
			}
			JSONObject json = ChatBotConfigFactory.getInstance().getQuestion(currentIdx);
			Object realAnswer = answer;
			if(currentChoice != null) {
				try	{
					
					 realAnswer = currentChoice.get(Integer.parseInt(answer)-1);
					 if(realAnswer == null) {
						 throw new Exception("잘못된 답변입니다. 다시 선택해 주세요.");
					 }
					 if(json.has("name")) {
						 if(realAnswer instanceof JSONObject ) {
							 String name = json.getString("name");
							 
							 String arr[] = name.split(",");
							 for(String a:arr) {
								 answerMap.put(a, getJSONStrVal( (JSONObject)realAnswer,a));
								 if(((JSONObject) realAnswer).has(a+"_NM")) {
									 answerMap.put(a+"_NM", getJSONStrVal( (JSONObject)realAnswer,a+"_NM"));
								 }
							 }
						 }
						 else
						 {
							 answerMap.put(json.get("name"), realAnswer);
						 }
					 }
					 if (realAnswer instanceof JSONObject ) {
						
						 JSONObject realAnswerJson = (JSONObject)realAnswer;
						 if(realAnswerJson.has("action")) {
							 doAction(realAnswerJson.getString("action"));
						 }
						 else if(realAnswerJson.has("go")) {
							 setCurrentIdx(realAnswerJson.getInt("go")-1);
						 }
						 else if(realAnswerJson.has("goback")) {
							 nextIdx = currentIdx;
							 setCurrentIdx(realAnswerJson.getInt("goback")-1);
						 }
						 else if(nextIdx != -1) {
								 setCurrentIdx(nextIdx-1);
								 nextIdx = -1;
							  
						 }
					 }
					 else if(nextIdx != -1) {
						 setCurrentIdx(nextIdx-1);
						 nextIdx = -1;
					 }
				}
				catch(Exception e) {
					e.printStackTrace();
					JSONObject result = new JSONObject();
					String msg = e.getMessage();
					if(("null".equals(msg) || msg==null) && e.getCause() != null) {
						msg = e.getCause().getMessage();
					}
					result.put("title", "[에러]" + e.getMessage()) ;
					result.put("id", currentIdx);
					return result ;
				}
			}
			else {
				if(json.has("name")) {
			
					answerMap.put(json.get("name"), realAnswer);
				}
				if(nextIdx != -1) {
					 setCurrentIdx(nextIdx-1);
					 nextIdx = -1;
				}
			}
			 
			currentIdx++;
			
		}
		return getQuestion();
	}
	private Object getChoice(  String url) throws Exception{
		
		
		String param = client.urlEncodeUTF8(answerMap);
		Object o = client.post(url, param);
		if(o instanceof JSONArray) {
			return (JSONArray)o;
			
		}
		if(o instanceof JSONObject) {
			JSONObject json = (JSONObject)o;
			if(json.has("list")) {
				return ((JSONObject)o).getJSONArray("list");
			}
			else {
				return json;
			}
		}
		throw new Exception("choice must be jsonarray" + o);
	}
	protected String getTitle(String title) {
		int pos = title.indexOf("${");
		if(pos>=0) {
			  Set<String> keys = this.answerMap.keySet();
			  for(String key:keys) {
				  String v  = "";
				  if(answerMap.get(key) != null) {
					  v = answerMap.get(key).toString();
				  }
				  title = TextHelper.replace(title, "${" + key + "}",  v);
				  if( title.indexOf("${")<0) {
					  break;
				  }
			  }
			
		}
		return title;
		
	}
	protected String getTitle(JSONObject map, String title) {
		int pos = title.indexOf("${");
		if(pos>=0) {
			  Set<String> keys = map.keySet();
			  for(String key:keys) {
				  String v  = "";
				  if(map.get(key) != null) {
					  v = map.get(key).toString();
				  }
				  title = TextHelper.replace(title, "${" + key + "}",  v);
				  if( title.indexOf("${")<0) {
					  break;
				  }
			  }
			
		}
		return title;
		
	}
	private JSONObject getQuestion() throws Exception {
		if( ChatBotConfigFactory.getInstance().getQuestionCount()<=currentIdx) {
			currentIdx = 0;
		}
		JSONObject json = ChatBotConfigFactory.getInstance().getQuestion(currentIdx);
		 
		JSONObject result = new JSONObject();
		result.put("title",getTitle( json.getString("title")));
		result.put("id", currentIdx);
		Object choice = null;
		if(json.has("listUrl")) {
			choice = getChoice(  json.getString("listUrl"));
		}
		else if(json.has("list")) {
			choice = json.getJSONArray("list");
		}
		if(choice != null) {
			currentChoice = new ArrayList();
			JSONArray itemList = new JSONArray();
			  
			if(choice instanceof JSONArray) {
				JSONArray list = (JSONArray)choice;
				for(int i=0; i < list.length(); i++) {
					if(json.has("iconKey")) {
						JSONObject item = new JSONObject();
						item.put("icon",  getJSONStrVal(list.getJSONObject(i),json.getString("iconKey")));
						item.put("icon_bgcolor",  getJSONStrVal(list.getJSONObject(i),json.getString("iconBgKey")));
						item.put("title",  getTitle(  getJSONStrVal(list.getJSONObject(i),json.getString("titleKey"))));
						if(json.has("desc")) {
							item.put("desc",getTitle(  list.getJSONObject(i), json.getString("desc")) );
						}
						else if(json.has("descKey")) {
							item.put("desc",  getJSONStrVal(list.getJSONObject(i),json.getString("descKey")));
						}
						itemList.put(item);
						currentChoice.add(list.getJSONObject(i));
					}
					else if(json.has("titleKey")) {
						currentChoice.add(list.getJSONObject(i));
						String title = getTitle(   list.getJSONObject(i).getString(json.getString("titleKey")) );
						if(json.has("color")) {
							JSONObject item = new JSONObject();
							item.put("title", title);
							item.put("color", json.getString("color"));
						}
						else {
							itemList.put(  title );
						}
					}
					else {
						currentChoice.add(list.get(i));
						itemList.put(  list.get(i) );
					}
					
				}
			}
			else   {
				JSONObject list = (JSONObject)choice;
				Iterator it = list.keys();
				int i=0;
				while(it.hasNext())	{
					String key = (String)it.next();
					JSONObject o = new JSONObject();
					o.put(json.getString("name"), key);
					o.put(json.getString("name") + "_NM", list.getString(key));
					currentChoice.add(o);
					itemList.put(  list.getString(key));
					i++;
					
				}
			}
			if(currentChoice.size()==0 && json.has("noListMsg")) {
				result.put("title",json.getString("noListMsg"));
			}
			else {
				result.put("list", itemList);
				if(json.has("iconKey")) {
					result.put("type", "img_select");
				}
				else {
					result.put("type", "select");
				}
			}
		}
		else {
			if(currentIdx < ChatBotConfigFactory.getInstance().getQuestionCount()-1) {
				result.put("type", "input");
			}
			currentChoice = null;
		}
		return result ;
	}
	public static String getJSONStrVal(JSONObject json, String key) {
		try {
			return json.get(key).toString();
		}
		catch(Exception e) {
			return "";
				
		}
	}
	public static JSONObject conversation(String sid, UserVO user, String answer) throws Exception {
		ChatBotBean bean = (ChatBotBean) user.getMemberData().get(ChatBotBean.class.getName());
		if(bean == null) {
			bean = new ChatBotBean(sid);
			user.getMemberData().put(ChatBotBean.class.getName(), bean);
		}
		return bean.answer( answer);
	}
		
}
