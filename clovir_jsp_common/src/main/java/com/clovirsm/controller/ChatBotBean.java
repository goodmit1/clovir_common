package com.clovirsm.controller;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.common.chatbot.ChatBotConfigFactory;
import com.clovirsm.hv.RestClient;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class ChatBotBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	int currentIdx = 0 ;
	List currentChoice;
	Map answerMap;
	RestClient client;
	public ChatBotBean(String sessionId) throws Exception {
		answerMap = new HashMap();
		client = new RestClient(ChatBotConfigFactory.getInstance().getBaseUrl());
		client.setMimeType("application/x-www-form-urlencoded");
		Map header = new HashMap();
		header.put("Cookie","JSESSIONID=" + sessionId);
		client.setHeader(header);
	}
	public int getCurrentIdx() {
		return currentIdx;
	}
	public void setCurrentIdx(int currentIdx) {
		this.currentIdx = currentIdx;
	}
	public List getCurrentChoice() {
		return currentChoice;
	}
	public void setCurrentChoice(List currentChoice) {
		this.currentChoice = currentChoice;
	}
	public Map getAnswer() {
		return answerMap;
	}
	public void setAnswer(Map answer) {
		this.answerMap = answer;
	}
	public void doAction(String action) throws  Exception {
		int pos = action.lastIndexOf(".");
		Object bean = SpringBeanUtil.getBean(action.substring(0, pos));
		bean.getClass().getMethod(action.substring(pos+1), Map.class).invoke(bean, answerMap);
	}
	public String answer(String answer) throws Exception {
		if(answer != null && !"".equals(answer)) {
			if("0".equals(answer)) {
				setCurrentIdx(-1);
				return  getQuestion();
			}
			JSONObject json = ChatBotConfigFactory.getInstance().getQuestion(currentIdx);
			Object realAnswer = answer;
			if(currentChoice != null) {
				try	{
					 realAnswer = currentChoice.get(Integer.parseInt(answer)-1);
					 if(json.has("name")) {
						 if(realAnswer instanceof JSONObject ) {
							 String name = json.getString("name");
							 
							 String arr[] = name.split(",");
							 for(String a:arr) {
								 answerMap.put(a, ((JSONObject)realAnswer).get(a));
							 }
						 }
						 else
						 {
							 answerMap.put(json.get("name"), realAnswer);
						 }
					 }
					 if (realAnswer instanceof JSONObject ) {
						 JSONObject realAnswerJson = (JSONObject)realAnswer;
						 if(realAnswerJson.has("action")) {
							 doAction(realAnswerJson.getString("action"));
						 }
						 else if(realAnswerJson.has("go")) {
							 setCurrentIdx(realAnswerJson.getInt("go")-1);
						 }
					 }
				}
				catch(Exception e) {
					return "�߸� �Է��ϼ̽��ϴ�. �ٽ� �Է��� �ּ���";
				}
			}
			else  if(json.has("name")) {
				answerMap.put(json.get("name"), realAnswer);
			}
			currentIdx++;
		}
		return getQuestion();
	}
	private Object getChoice(  String url) throws Exception{
		
		
		String param = client.urlEncodeUTF8(answerMap);
		Object o = client.post(url, param);
		if(o instanceof JSONArray) {
			return (JSONArray)o;
			
		}
		if(o instanceof JSONObject) {
			JSONObject json = (JSONObject)o;
			if(json.has("list")) {
				return ((JSONObject)o).getJSONArray("list");
			}
			else {
				return json;
			}
		}
		throw new Exception("choice must be jsonarray" + o);
	}
	private String getQuestion() throws Exception {
		if( ChatBotConfigFactory.getInstance().getQuestionCount()<=currentIdx) {
			currentIdx = 0;
		}
		JSONObject json = ChatBotConfigFactory.getInstance().getQuestion(currentIdx);
		String title = json.getString("title");
		Object choice = null;
		if(json.has("listUrl")) {
			choice = getChoice(  json.getString("listUrl"));
		}
		else if(json.has("list")) {
			choice = json.getJSONArray("list");
		}
		if(choice != null) {
			currentChoice = new ArrayList();
			
			title += "\n";
			if(choice instanceof JSONArray) {
				JSONArray list = (JSONArray)choice;
				for(int i=0; i < list.length(); i++) {
					if(json.has("titleKey")) {
						currentChoice.add(list.getJSONObject(i));
						title += (i+1) + ":" + list.getJSONObject(i).getString(json.getString("titleKey")) + "\n";
					}
					else {
						currentChoice.add(list.get(i));
						title += (i+1) + ":" + list.get(i) + "\n";
					}
					
				}
			}
			else   {
				JSONObject list = (JSONObject)choice;
				Iterator it = list.keys();
				int i=0;
				while(it.hasNext())	{
					String key = (String)it.next();
					currentChoice.add(key);
					title += (i+1) + ":" + list.getString(key) + "\n";
					i++;
					
				}
			}
			if(currentChoice.size()==0 && json.has("noListMsg")) {
				title = json.getString("noListMsg");
			}
			
		}
		else {
			currentChoice = null;
		}
		return title;
	}
	

}
