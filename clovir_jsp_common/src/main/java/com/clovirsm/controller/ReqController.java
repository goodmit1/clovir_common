package com.clovirsm.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCReqService;
import com.fliconz.fm.common.cache.MessageBundle;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.ListUtil;
import com.fliconz.fw.runtime.util.MapUtil;

public abstract class ReqController extends DefaultController {


	/**
	 * �떊洹� �슂泥�, DEPLOY_REQ_YN=Y�씠硫� 諛붾줈 �슂泥�
	 * @throws Exception
	 */
	@RequestMapping(value =  "/insertReq" )
	public Object insertReq_request(HttpServletRequest request) throws Exception
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				boolean isDeploy="Y".equals(param.get("DEPLOY_REQ_YN"));
				int row= getNCReqService().insertReq1(param,isDeploy);

				return ControllerUtil.getSuccessMap(param,  "INS_DT");

			}

		}).run(request);



	}
	/**
	 * �닔�젙 �슂泥�, DEPLOY_REQ_YN=Y�씠硫� 諛붾줈 �슂泥�
	 * @throws Exception
	 */
	@RequestMapping(value =  "/updateReq" )
	public Object updateReq_request(HttpServletRequest request) throws Exception
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				boolean isDeploy="Y".equals(param.get("DEPLOY_REQ_YN"));
				int row= getNCReqService().updateReq1(param,isDeploy);

				return ControllerUtil.getSuccessMap(param,  "INS_DT");

			}

		}).run(request);




	}

	/**
	 * �궘�젣 �슂泥�, DEPLOY_REQ_YN=Y�씠硫� 諛붾줈 �슂泥�
	 * @throws Exception
	 */
	@RequestMapping(value =  "/deleteReq" )
	public Object deleteReq_request(HttpServletRequest request) throws Exception
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				boolean isDeploy="Y".equals(param.get("DEPLOY_REQ_YN"));
				int row= getNCReqService().deleteReq1(param,isDeploy);

				return ControllerUtil.getSuccessMap(param,  "INS_DT");

			}

		}).run(request);




	}
	@RequestMapping(value =  "/deleteReq_multi" )
	public Object deleteReqMulti_request(HttpServletRequest request) throws Exception
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				int row =  saveReqs(param,"D");

				return ControllerUtil.getSuccessMap(param,  "INS_DT");

			}

		}).run(request);




	}
	
	private int saveReqs(Map param , String cudCd) throws  Exception {
		String jsonArrStr = (String)param.get("selected_json");
		if(jsonArrStr == null || "".equals(jsonArrStr)) throw new Exception("NO DATA");
		JSONArray arr = new JSONArray(jsonArrStr);
		List<Map> selRows = new ArrayList();
		for(int i=0; i < arr.length(); i++)
		{
			selRows.add( MapUtil.jsonToMap( arr.getJSONObject(i).toString()) );
		}
		 
		boolean isDeploy="Y".equals(param.get("DEPLOY_REQ_YN"));
		return getNCReqService().saveReqs(selRows,cudCd, isDeploy);
	}
	@RequestMapping(value =  "/updateReq_multi" )
	public Object updateReqMulti_request(HttpServletRequest request) throws Exception
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				int row =  saveReqs(param,"U");

				return ControllerUtil.getSuccessMap(param,  "INS_DT");

			}

		}).run(request);




	}

	protected NCReqService getNCReqService()
	{
		return (NCReqService)getService();
	}



	@RequestMapping(value =  "/chg_owner" )
	public Object changeOwner(HttpServletRequest request) throws Exception
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map selRow) throws Exception {

				String pkKey = getNCReqService().getPks()[0];
				String pkVal = (String) selRow.get(pkKey);
				String owner= (String)selRow.get("NEW_USER");
				if(pkVal != null && owner != null)
				{
					getNCReqService().updateOwner(  pkVal, owner );
				}
				return ControllerUtil.getSuccessMap(selRow, pkKey);

			}

		}).run(request);


	}

	@RequestMapping(value =  "/chg_owner_pk_multi" )
	public Object changeOwnerMulti(HttpServletRequest request) throws Exception
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map selRow) throws Exception {
				System.out.println("***********************");
				System.out.println(getNCReqService().getPks());
				String[] pkKey = getNCReqService().getPks();
				List<String> pkVal = new ArrayList();
				for(int i = 0 ; i < pkKey.length ; i++) {
					pkVal.add((String)selRow.get(pkKey[i]));
				}
				String owner= (String)selRow.get("NEW_USER");
				if(pkVal != null && owner != null)
				{
					getNCReqService().updateOwnerPKMulti(  pkVal, owner );
				}
				return ControllerUtil.getSuccessMap(selRow, pkKey);

			}

		}).run(request);


	}
}
