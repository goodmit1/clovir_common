package com.clovirsm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.ScheduleJspService;
import com.fliconz.fm.mvc.DefaultController;

@RestController
@RequestMapping(value =  "/schedule" )
public class ScheduleJspController extends DefaultController {
	
	@Autowired ScheduleJspService service;

	@Override
	protected ScheduleJspService getService() {
		return service;
	}

}