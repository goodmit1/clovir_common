package com.clovirsm.controller.admin;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.EtcFeeMngService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.util.ControllerUtil;

@RestController
@RequestMapping(value =  "/etc_fee_mng" )
public class EtcFeeMngController extends DefaultController {

	@Autowired EtcFeeMngService service;
	@Override
	protected EtcFeeMngService getService() {
		 
		return service;
	}
	
	
	@RequestMapping(value =  "/etcDetail" )
	public Object etcDetail(final HttpServletRequest request, HttpServletResponse response) {
		return  (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				return service.getEtcFeeDetail(param);
			}
		}).run(request);
	}

	@RequestMapping(value =  "/etcDetailSaveMulti" )
	public void etcDetailSaveMulti(final HttpServletRequest request, HttpServletResponse response) {
		Map param;
		try {
			param = ControllerUtil.getParam(request);
			service.etcDetailSaveMulti(param);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
