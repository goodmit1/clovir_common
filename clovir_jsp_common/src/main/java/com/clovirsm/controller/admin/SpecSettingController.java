package com.clovirsm.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.SpecSettingService;
import com.fliconz.fm.mvc.DefaultController;

@RestController
@RequestMapping(value =  "/spec_setting" )
public class SpecSettingController extends DefaultController {

	@Autowired SpecSettingService service;
	@Override
	protected SpecSettingService getService() {
		return service;
	}

}
