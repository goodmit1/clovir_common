package com.clovirsm.controller.admin;

import com.clovirsm.service.admin.UserApprovalService;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user_approval")
public class UserApprovalController extends DefaultController {

    @Autowired
    UserApprovalService userApproveService;

    @Override
    protected DefaultService getService() {
        return userApproveService;
    }

}
