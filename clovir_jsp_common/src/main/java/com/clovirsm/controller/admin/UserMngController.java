package com.clovirsm.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.mvc.service.MiscService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.util.EscapeUtil;
import org.apache.velocity.anakia.Escape;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.UserSearchService;
import com.fliconz.fm.admin.service.UserService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.security.UserVO;

@RestController
@RequestMapping(value =  "/user_mng" )
public class UserMngController extends DefaultController {

	@Autowired
	transient UserService userService;

	@Autowired UserSearchService service;

	@Override
	protected UserSearchService getService() {
		return service;
	}

	@RequestMapping(value =  "/passwordReset" )
	public Map passwordReset(final HttpServletRequest request, HttpServletResponse response) throws Exception {
		return ((Map) new ActionRunner() {
			protected Object run(Map param) throws Exception {
				param.put("PASSWORD", param.get("LOGIN_ID"));
				userService.chgPassword(param);
				return new HashMap();
			}
		}.run(request));
	}

	@RequestMapping(value =  "/querykey/{key}" )
	public Object getListByQueryKey(final HttpServletRequest request, HttpServletResponse response, @PathVariable("key") final String key) {
		return  (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				List list = getService().selectListByQueryKey(key, param);
				Map tmp = new HashMap();
				tmp.put("list", list);
				return tmp;
			}
		}).run(request);

	}

	@RequestMapping(value = "/findID")
	public int findID(HttpServletRequest request) {
		return service.findID(request);
	}

	@RequestMapping(value = "/findPassword")
	public int findPassword(HttpServletRequest request) {
		return service.findPassword(request);
	}
}