package com.clovirsm.controller.popup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.popup.CategorySearchPopupService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;

@RestController
@RequestMapping(value =  "/popup/category" )
public class CategorySearchPopupController extends DefaultController{

	@Autowired CategorySearchPopupService service;
	@Override
	protected DefaultService getService() {
		return service;
	}

	@Override
	@RequestMapping({"/delete"})
	public Map delete(HttpServletRequest request, HttpServletResponse response) {
		return ((Map) new ActionRunner() {
			protected Object run(Map param) throws Exception {
				List<String> list = new ArrayList(Arrays.asList(((String) param.get("DELETE_LIST")).split(",")));
				param.put("DELETE_LIST", list);
				getService().delete(param);
				 
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}
		}.run(request));
	}

}
