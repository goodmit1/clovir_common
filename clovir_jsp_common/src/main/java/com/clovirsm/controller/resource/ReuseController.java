package com.clovirsm.controller.resource;
 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.controller.ReqController;
import com.clovirsm.service.resource.ExtendPeriodService;
import com.clovirsm.service.resource.ReuseService;
import com.fliconz.fm.mvc.DefaultService;

@RestController
@RequestMapping(value =  "/reuse" )
public class ReuseController extends ReqController{

	@Autowired @Qualifier("reuseService") ReuseService service;
	@Override
	protected DefaultService getService() {
		return service;
	}

}
