package com.clovirsm.controller.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.workflow.ApprovalHistoryService;
import com.clovirsm.service.workflow.NextApproverService;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;

@RestController
@RequestMapping(value =  "/nextApprover" )
public class NextApproverController extends DefaultController {

	
	@Autowired NextApproverService service;
	@Override
	protected DefaultService getService() {
		return service;
	}
}
