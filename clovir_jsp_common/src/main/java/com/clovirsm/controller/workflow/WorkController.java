package com.clovirsm.controller.workflow;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.workflow.WorkService;
import com.clovirsm.util.JSONUtil;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.util.ControllerUtil;

@RestController
@RequestMapping(value =  "/work" )
public class WorkController extends DefaultController{

	@Autowired WorkService service;
	@Override
	protected WorkService getService() {
		return service;
	}


	@RequestMapping(value =  "/request" )
	public Object  request(final HttpServletRequest request, HttpServletResponse response){
		return  (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				JSONArray checkedArray = JSONUtil.createJSONArray((String)param.get("checked"));
				List<Map> checkedRow = new ArrayList<Map>(checkedArray);

				Map selRow = (Map)JSONUtil.createJSON((String)param.get("selRow"));
				getService().insertAuthRequest(checkedRow, selRow);
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}
		}).run(request);
	}

	@RequestMapping(value =  "/cancel" )
	public Object cancel(final HttpServletRequest request, HttpServletResponse response){
		return  (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				JSONArray checkedArray = JSONUtil.createJSONArray((String)param.get("checked"));
				Set<Map> checkedRow = new HashSet<Map>(checkedArray);
				getService().updateWorkCancel(checkedRow);
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}
		}).run(request);
	}

}
