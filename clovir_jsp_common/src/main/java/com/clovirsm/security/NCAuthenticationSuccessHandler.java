package com.clovirsm.security;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.clovirsm.common.IPAddressUtil;
import com.clovirsm.service.ComponentService;
import com.fliconz.fm.mvc.security.LoginSuccessHandler;
import com.fliconz.fm.security.FMAuthenticationSuccessHandler;
import com.fliconz.fm.security.UserVO;


public class NCAuthenticationSuccessHandler extends LoginSuccessHandler {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		super.onAuthenticationSuccess(request, response, authentication);
		UserVO userVo = (UserVO) authentication.getPrincipal();
		String admin_ips = (String)ComponentService.getEnv("admin_ips", "");
		if(!"".equals(admin_ips) && userVo.hasRole("ROLE_PORTAL_MANAGER")) {
			boolean isFind = false;
			List<String> myIps = IPAddressUtil.getLocalServerIp();
			for(String ip:myIps) {
				if(ip.matches(admin_ips)) {
					isFind = true;
				}

			}
			if(!isFind) {
				System.out.println(myIps);
				request.getSession().invalidate();
				throw new ServletException("Admin IP is not available");
			}
		}
	}
}
