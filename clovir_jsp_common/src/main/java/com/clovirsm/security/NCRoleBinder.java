package com.clovirsm.security;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import com.fliconz.fm.log.LogManager;
import com.fliconz.fm.security.IRoleBinder;
import com.fliconz.fm.security.UserDetailService;
import com.fliconz.fm.security.UserVO;

@Service(value="NCRoleBinder")
public class NCRoleBinder implements IRoleBinder {

	@Override
	public void bindRole(UserVO userVO, List<SimpleGrantedAuthority> roles, UserDetailService userDetailService) throws Exception {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("USER_ID", userVO.getUserId());
		Map resultMap = userDetailService.getDAO().selectOne("com.clovirsm.security.hasNotGroupUser", param);
		long hasGroup = (Long)resultMap.get("HAS_GROUP");
		long isGroupManager = (Long)resultMap.get("IS_GROUP_MANAGER");
		if(hasGroup == 0 && isGroupManager == 0) {
			roles.add(new SimpleGrantedAuthority(NCSecurityConstant.ROLE_NO_GRP_MEMBER));
			LogManager.getAuthenticationLogger().debug("LOGIN_ID=["+userVO.getUserName()+"], USER_ID=["+userVO.getUserId()+"], ROLE_NM=["+NCSecurityConstant.ROLE_NO_GRP_MEMBER+"] DEFINED");
		}
	}
}
