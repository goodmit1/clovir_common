package com.clovirsm.security.util;

import java.util.Map;

import javax.inject.Singleton;

import org.springframework.stereotype.Component;

import com.fliconz.fm.security.util.FMKeyGenerator;
import com.fliconz.fw.runtime.util.PropertyManager;

@Singleton
@Component("keyGenerator")
public class NCKeyGenerator extends FMKeyGenerator{

	@Override
	public String generateKey(Map<String, Object> paramMap) {
		return paramMap.get(PropertyManager.getString("generator.key.name","key")).toString() + System.nanoTime();
	}

	@Override
	public void beforeInsert(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
	}

	@Override
	public void afterInsert(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
	}

	@Override
	public void beforeUpdate(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
	}

	@Override
	public void afterUpdate(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
	}

	@Override
	public void beforeDelete(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
	}

	@Override
	public void afterDelete(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
	}

}
