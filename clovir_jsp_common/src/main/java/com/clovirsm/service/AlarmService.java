
package com.clovirsm.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCConstant;
import com.clovirsm.service.workflow.DefaultNextApprover;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Service
public class AlarmService extends DefaultService {

	public enum TYPE {
		C, //생성
		U, // 변경
		D, // 삭제
		EXPIRE, //만료
		EXPIRE_WARN, //만료예정
		E, // 기간 연장
		REUSE, // 복원
		SVR_START, // 서버 시작
		SVR_STOP, // 서버 중지
		SVR_NAS_UN, // 서버 NAS unmount
		SVR_OS, //서버  OS 변경
		OWNER, //owner 변경
		}
	@Override
	protected String getTableName() {
		return "NC_ALARM";
	}

	@Override
	public String[] getPks() {
		// TODO Auto-generated method stub
		return new String[] {"ALARM_ID"};
	}

	@Override
	protected String getNameSpace() {

		return "com.clovirsm.alarm";
	 
	}
 
	public void logAlarm(String msg, String type, Object svcId  )   {
		try {
			insertAlarm(msg, type, svcId, null );
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}
	public boolean insertAlarmChkTerm(String msg, String type, Object svcId, int term, Object... rcvs  ) {	
		Map paramA = new HashMap();
		paramA.put("ALARM_MSG", msg);
		paramA.put("ALARM_TYPE", type);
		paramA.put("ALARM_SVC_ID", svcId);
		paramA.put("TERM_DAY", term);
		try {
			int cnt = this.selectCountByQueryKey("selectCountByTerm", paramA);
			if(cnt>0) {
				return false;
			}
			insertAlarm(  msg,   type,   svcId,   rcvs  );
			return true;
		} catch (Exception e) {
			 
			e.printStackTrace();
			return false;
		}
	}
 
	public void insertAlarm(String msg, String type, Object svcId,Object insId, Object... rcvs  ) {	
		Map paramA = new HashMap();
		DefaultNextApprover approver = (DefaultNextApprover)SpringBeanUtil.getBean("nextApprover"); 
		Long id = Long.valueOf(System.nanoTime());
		paramA.put("ALARM_ID", id);
		paramA.put("ALARM_MSG", msg);
		paramA.put("ALARM_TYPE", type);
		paramA.put("ALARM_SVC_ID", svcId);
		if(insId != null) {
			paramA.put("INS_ID",insId );
		}
		try {
			this.insert(paramA);
			
			if(rcvs != null) {
				for(Object obj : rcvs) {
					Map paramU = new HashMap();
					paramU.put("ALARM_ID", id);
					paramU.put("USER_ID", obj);
					
					this.insertByQueryKey("insert_NC_ALARM_RCV", paramU);
				}
			}
			 
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}
}
