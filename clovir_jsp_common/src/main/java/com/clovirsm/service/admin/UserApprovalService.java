package com.clovirsm.service.admin;

import com.clovirsm.common.NCDefaultService;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.PropertyManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserApprovalService extends NCDefaultService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SaltSource saltSource;

    @Override
    protected String getTableName() {
        return "FM_USER";
    }

    @Override
    public String[] getPks() {
        return new String[]{"USER_ID"};
    }

    @Override
    protected int insertDBTable(String tableNm, Map param) throws Exception {
        String hashedPassword = (String)param.get("LOGIN_ID");
        UserVO vo = new UserVO(param);
        boolean isEncrypted = PropertyManager.getBoolean("password.security.encryption", false);
        if (isEncrypted)
            hashedPassword = passwordEncoder.encodePassword(hashedPassword, saltSource.getSalt(vo));
        else {
            hashedPassword = hashedPassword;
        }
        param.put("PASSWORD", hashedPassword);
        return super.insertDBTable(tableNm, param);
    }

    @Override
    protected Map<String, String> getCodeConfig() {
        Map<String, String> conf = new HashMap<String, String>();
        conf.put("USE_YN", "USER_APPROVAL");
        return conf;
    }

    @Override
    protected String getNameSpace() {
        return "com.clovirsm.admin.userApproval";
    }
}
