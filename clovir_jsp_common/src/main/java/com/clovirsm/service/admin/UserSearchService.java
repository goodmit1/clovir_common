package com.clovirsm.service.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.mvc.service.MiscService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.security.password.PasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.PropertyManager;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserSearchService extends NCDefaultService {


	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private SaltSource saltSource;

	@Autowired
	MiscService miscService;

	@Autowired
	PasswordService passwordService;

	protected String getNameSpace() {
		return "com.clovirsm.admin.userMng.UserSearch";
	}

	@Override
	public String[] getPks() {
		return new String[]{"USER_ID"};
	}

	@Override
	protected String getTableName() {
		return "FM_USER";
	}

	
	
	@Override
	public List list(Map searchParam) throws Exception {
		// TODO Auto-generated method stub
		List  result = super.list(searchParam);
		System.out.println("user list: "+result);
		return result;
	}

	@Override
	protected int insertDBTable(String tableNm, Map param) throws Exception {
		String hashedPassword = (String)param.get("LOGIN_ID");
		UserVO vo = new UserVO(param);
		boolean isEncrypted = PropertyManager.getBoolean("password.security.encryption", false);
		if (isEncrypted)
			hashedPassword = passwordEncoder.encodePassword(hashedPassword, saltSource.getSalt(vo));
		else {
			hashedPassword = hashedPassword;
		}
		param.put("PASSWORD", hashedPassword);
		return super.insertDBTable(tableNm, param);
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map<String, String> conf = new HashMap<String, String>();
		conf.put("USER_TYPE", "USER_TYPE");
		conf.put("USE_YN", "sys.yn");
	 
		return conf;
	}

	public int findID(HttpServletRequest request) {
		try {
			Map param = ControllerUtil.getParam(request);
			List<Map> userRes = this.selectListByQueryKey("selectByEmail_FM_USER", param);

			if(userRes.size() < 1) { return -2;}

			this.miscService.sendMail((String) userRes.get(0).get("EMAIL"), (String) param.get("title"), (String) param.get("template"), "ko", userRes.get(0));
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int findPassword(HttpServletRequest request) {
		try {
			Map param = ControllerUtil.getParam(request);
			List<Map> userRes = this.selectListByQueryKey("selectByEmail_FM_USER", param);

			if(userRes.size() < 1) { return -2;}

			passwordService.requestEmail( (String) userRes.get(0).get("LOGIN_ID"), (String) userRes.get(0).get("EMAIL") );
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

}