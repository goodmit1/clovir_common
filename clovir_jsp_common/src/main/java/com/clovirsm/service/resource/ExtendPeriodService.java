package com.clovirsm.service.resource;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCReqService;
import com.clovirsm.service.workflow.ExpireService;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.fliconz.fw.runtime.util.NumberUtil;

@Service 
public class ExtendPeriodService extends NCReqService {

	@Autowired ExpireService expireService;
	@Override
	protected String getDefaultTitle(Map param) {
	 
		return  this.getCodeMap("SVC", this.getLang()).get(param.get("SVC_CD"))   + " " + param.get("TITLE") + " " + ( "V".equals((String) param.get("SVC_TYPE"))? MsgUtil.getMsg("title_server_reuse", new String[] {}) : MsgUtil.getMsg("title_extendPeriod", new String[] {param.get("ADD_USE_MM") != null ? param.get("ADD_USE_MM").toString() : param.get("EXPIRE_DT").toString()}));
		
	}

	@Override
	protected boolean isSavable(Map selRow) throws Exception
	{
		return true;
	}
	@Override
	protected boolean onDeploy(String pkVal, String cudCode, Map info) throws Exception {
		String svcCd = (String) info.get("SVC_CD");
	 
		NCReqService reqService = NCReqService.getService(svcCd);
		info.putAll(reqService.addTableParam(pkVal, false));
		this.updateByQueryKey("update_USE_MM", info);
		 
		return true;
	}

	@Override
	public String getSVC_CD() {
		return NCConstant.SVC.E.toString();
	}

	@Override
	protected String getTableName() {
		 
		return "NC_EXTEND_PERIOD";
	}

	@Override
	public String[] getPks() {
	 
		return new String[] {"SVC_ID"};
	}

	@Override
	protected String getNameSpace() {
		 
		return "com.clovirsm.resource.extendPeriod";
	}

}
