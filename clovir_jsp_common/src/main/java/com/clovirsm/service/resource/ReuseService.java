package com.clovirsm.service.resource;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCReqService;
import com.clovirsm.service.workflow.ExpireService;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.fliconz.fw.runtime.util.NumberUtil;

@Service 
public class ReuseService extends NCReqService  {

	private @Autowired ExpireService expireService;

	@Override
	protected boolean isSavable(Map selRow) throws Exception
	{
		return true;
	}
	 
	@Override
	protected String getTableName() {
		 
		return "NC_EXTEND_PERIOD";
	}

	@Override
	public String[] getPks() {
	 
		return new String[] {"SVC_ID"};
	}

	@Override
	protected String getNameSpace() {
		 
		return "com.clovirsm.resource.extendPeriod";
	} 
	@Override
	protected String getDefaultTitle(Map param) {
	 
		return param.get("TITLE") + " " +   MsgUtil.getMsg("title_server_reuse", new String[] {})  ;
	}

	 
	@Override
	protected boolean onDeploy(String pkVal, String cudCode, Map info) throws Exception {
		String svcCd = (String) info.get("SVC_CD");
		expireService.updateReUse(svcCd, pkVal, NumberUtil.getInt(info.get("ADD_USE_MM") != null ? info.get("ADD_USE_MM") : 0), (Date)info.get("EXPIRE_DT"));
		 
		return true;
	}

	@Override
	public String getSVC_CD() {
		return NCConstant.SVC.V.toString();
	}

	 
}
