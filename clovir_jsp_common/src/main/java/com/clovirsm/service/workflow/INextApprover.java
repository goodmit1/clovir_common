package com.clovirsm.service.workflow;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface INextApprover {

	StepInfo getNextApprover(int step, Map param) throws Exception;
	public List<String> getAllStepNames();
	public boolean isApprover();
	public void onAfterDeny(Map param) throws Exception;
	 
	 
	 
	
	void onAfterRequest(List approverList, Map param) throws Exception;
	 
	public List getMemberByRole(String roleName, Map param) throws Exception;
	void onAfterDeploy(Map param, List<Map> details) throws Exception;
	 
	public void notiAlarm(String svcCd, String type, String svcId, Map param) throws Exception;
	 
	/**
	 * reqId�� �ְų�, data�� �ְų�
	 * @param reqId
	 * @param data
	 * @return
	 */
	public String getDetailMailContent(  String reqId , Map data) ;
	Map getDetailList(String svcCd, String svcId, String insDt) throws Exception;
	String chkBeforeDeploy(List<Map> details) throws Exception;
}
