package com.clovirsm.service.workflow;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fliconz.fm.common.util.NumberHelper;
import com.fliconz.fm.mvc.util.MessageException;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCDefaultService;
 

@Service
public class NextApproverService extends NCDefaultService {

	public static final String NS="com.clovirsm.workflow.approver";
	
	@Autowired INextApprover nextApprover;
	protected String getNameSpace() {
		return NS;
	}

	public INextApprover getNextApproverAPI() {
		return nextApprover;
	}
	public boolean isAutoDeploy(Map paramMap) throws Exception
	{
		boolean isApprovalAuto = PropertyManager.getBoolean("clovirsm.approval.auto", true);
		if(isApprovalAuto)
		{
			paramMap.put(StepInfo.PARAM_STEP,0);
			StepInfo stepInfo = this.getNextStepInfo(paramMap);
			return stepInfo==null || stepInfo.isEnd();
		}
		return false;
	}
	
	public Map getListName() throws Exception
	{	
		Map<String, String> pram = new HashMap<String, String>();
		String userId = String.valueOf(UserVO.getUser().getUserId());
		System.out.println("################################");
		System.out.println(UserVO.getUser().getUserId());
		System.out.println(userId);
		System.out.println("################################");
		pram.put("_USER_ID_", userId);
		
		return this.selectOneByQueryKey("nextListName", pram);
	}
	public boolean isApprover() throws Exception
	{
		return nextApprover.isApprover();
	}
	public List getAllStepNames() throws Exception
	{
		return nextApprover.getAllStepNames();
	}
	public StepInfo getNextStepInfo(Map paramMap) throws Exception
	{
		int step = NumberHelper.getInt(paramMap.get(StepInfo.PARAM_STEP));
		return nextApprover.getNextApprover(step, paramMap);
	}
	public StepInfo insertNextStepInfo(Map paramMap) throws Exception
	{
		int step = NumberHelper.getInt(paramMap.get(StepInfo.PARAM_STEP));
		// syk 2021.04.16 dblock this.deleteDBTable("NC_REQ_NEXT_APPROVER", paramMap);
		StepInfo nextStepInfo = nextApprover.getNextApprover(step, paramMap);
		
		if(nextStepInfo != null && !nextStepInfo.isEnd()) 
		{
			paramMap.put(StepInfo.PARAM_STEP, nextStepInfo.getStepIdx());
			List approverList = nextStepInfo.getApproverList();
			if(approverList != null && approverList.size()>0)
			{
				for(Object app: approverList)
				{
					paramMap.put("APPROVER", app);
					try {
						this.insertDBTable("NC_REQ_NEXT_APPROVER", paramMap);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
			else
			{
				throw new MessageException("NC_REQ_NO_APPROVER");	
			}
			paramMap.put("APPR_STATUS_CD", NCConstant.APP_KIND.R.toString());	
			this.nextApprover.onAfterRequest(approverList, paramMap);
		}
		else
		{
			
			paramMap.put(StepInfo.PARAM_STEP,  getAllStepNames().size() );
			paramMap.put("APPR_STATUS_CD", NCConstant.APP_KIND.A.toString());	
		}
	 
		return nextStepInfo;
	}

	@Override
	public String[] getPks() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return null;
	}

}
