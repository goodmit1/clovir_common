package com.clovirsm.service.workflow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;
import com.fliconz.fw.runtime.util.PropertyManager;

@Service
public class ReqHistoryService extends NCDefaultService {
	
	@Autowired
	DefaultNextApprover defaultNextApprover;
	
	@Override
	protected String getTableName() {
		 
		return "NC_REQ_DETAIL";
	}

	@Override
	public String[] getPks() {
	 
		return new String[] {"REQ_ID", "SVC_ID"};
	}

	@Override
	protected String getNameSpace() {
		 
		return "com.clovirsm.resouces.req_history";
	}
	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("CUD_CD", "CUD");
		config.put("SVC_CD", "SVC");
		config.put("SVCCUD","SVCCUD");
		config.put("APPR_STATUS_CD", "APP_KIND1");
		config.put("TASK_STATUS_CD", "TASK_STATUS_CD");
		return config;
	}
	
	@Override
	  public List list(Map searchParam) throws Exception
	  {
		List<String> workLine = defaultNextApprover.getAllStepNames();
		List<Map> list= super.list(searchParam);
		for(Map m : list) {
			int step = Integer.parseInt(m.get("STEP").toString());
			if(workLine != null && step != 0) {
				if( step < workLine.size()) {
					m.put("apprW", workLine.get(step));
				}
				else
					m.put("apprW", "");
			}
		}
	    return list;
	  }
}
