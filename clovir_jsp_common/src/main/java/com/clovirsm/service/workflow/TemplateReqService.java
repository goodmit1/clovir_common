package com.clovirsm.service.workflow;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.clovirsm.common.IDGenHelper;
import com.clovirsm.common.NCDefaultService; 
@Service
public class TemplateReqService extends NCDefaultService {

	 

	@Override
	protected String getTableName() {
		 
		return "NC_TEMPLATE_REQ";
	}

	@Override
	public String[] getPks() {
		 
		return new String[]{"TEMPLATE_ID"};
	}
	
	@Override
	protected String getNameSpace() {
		return "com.clovirsm.resources.templateReq";
	}
	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("TASK_STATUS_CD", "TASK_STATUS_CD1");
		return config;
	}
	@Override
	public int update( Map param) throws Exception {
		int row = super.update( param);
		 
		return row;
	}
	@Override
	protected int insertDBTable(String tableNm, Map param) throws Exception {
		if(tableNm.equals("NC_TEMPLATE_REQ")) { 
			param.put("TEMPLATE_ID", "" + System.nanoTime());
			param.put("TASK_STATUS_CD", "W");
		}
		int row = super.insertDBTable(tableNm, param);
		
		super.sendMailToAdmin("TITLE_ADD_TEMPLATE", "templateAdd", param);
		return row;
	}
	protected int updateDBTable(String tableNm, Map param) throws Exception
	  {
		int row = super.updateDBTable(tableNm, param);
		
		Map info = super.getInfo(param);
		
		super.sendMail(info.get("INS_ID"), "TITLE_REQ_TEMPLATE", "templateResult", info);
	    return row;
	  }
}