package com.clovirsm.sys.hv;

import javax.annotation.PostConstruct;

import org.apache.log4j.LogManager;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import com.clovirsm.service.AlarmService;
import com.clovirsm.service.ComponentService;
import com.clovirsm.service.workflow.ExpireService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
 

public class CommonScheduleService {
	@Autowired TaskCheckJob taskCheckJob;
	 
	@Autowired ComponentService compService;
	protected SqlSessionTemplate sqlSession;
	@Autowired
	protected	AlarmService alarmService ;
	public org.apache.log4j.Logger log = LogManager.getLogger(this.getClass());

	public SqlSessionTemplate getSqlSession() {
		return sqlSession;
	}

	public void setSqlSession(SqlSessionTemplate sqlSession) {
		this.sqlSession = sqlSession;
	}
	
	/**
	 * 회수 프로세스
	 */
	public void runExpire()
	{
		if( isRunServer())
		{	
			try
			{
				ExpireService service = (ExpireService)SpringBeanUtil.getBean("expireService");
				String mailDays = (String) ComponentService.getEnv("expire.mail.day", "-30");
				String[] days = mailDays.split(",");
				for(String d:days) {
					service.sendWarnMail(Integer.parseInt(d));
				}
				service.update_expire();
				String d = (String) ComponentService.getEnv("expire.delete.day", "30");
				service.delete((Integer.parseInt(d)));
			
			}catch (Exception e) {
				e.printStackTrace();
				log.error("EXPIRE", e);
				alarmService.logAlarm("expire error :" + e.getMessage() , "SCHEDULE", true );
			}
		}
	}
	@PostConstruct
	public void init()
	{
		 
	}
	 
	public boolean isRunServer()
	{
		 
		
		return compService.isScheduleService();
	}
	public void runTaskCheck() throws Exception 
	{
		if( isRunServer())
		{	
			taskCheckJob.run();
		}
	}
	public void doTaskCheck() throws Exception 
	{
		if( isRunServer())
		{	
			taskCheckJob.run();
		}
	}
}
