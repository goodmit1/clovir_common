package com.clovirsm.sys.hv;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.clovirsm.common.IAsyncJob;
import com.clovirsm.common.NCException;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.service.TaskService;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Component
public class TaskCheckJob {
	public void run() throws Exception
	{
		TaskService service = (TaskService)SpringBeanUtil.getBean("taskService");
		List<Map> running =  service.listRunningTask();
		 
		for(Map r : running)
		{
			IAfterProcess after = getAfter(  r);
			if(after !=null)
			{	
				after.chkState();
			}
			else {
				service.finishTask((String) r.get("TASK_ID"));
			}
		}
		
		 
		 
	}
	public static IAsyncJob getService(String svcCd) throws Exception
	{
		String svcName = PropertyManager.getString("clovirsm.req.service." + svcCd);
		if(svcName == null) throw new NCException("Service is not found. property name=[clovirsm.req.service." + svcCd+"]");
		return (IAsyncJob)SpringBeanUtil.getBean(svcName);
	}
	private IAfterProcess getAfter( Map r) throws Exception
	{
		
		String taskCd = (String)r.get("TASK_CD");
		
		IAsyncJob job = getService(taskCd);
		if(job != null)
		{
			IAfterProcess process =  job.getAfter(  (String)r.get("SVC_ID"));
			if(process != null) process.setTask((String) r.get("TASK_ID"));
			return process;
		}
		return null; 
		
		
	 
	
	}
}
