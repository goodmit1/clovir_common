package com.clovirsm.hv;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONObject;

public class CommonUtil {
	public static JSONArray getJSONArray(JSONObject json, String key) {
		Object o = json.get(key);
		JSONArray result = null;
		if(o instanceof JSONObject) {
			result = new JSONArray();
			result.put(o);
		}
		else {
			result = (JSONArray)o;
		}
		return result;
	}
	public static String replace(String value, String oldString, String newString) {
		int intFrom = 0;
		StringBuilder sb = new StringBuilder();
		int intTo = 0;
		do {
			intTo = value.indexOf(oldString, intFrom);
			if(intTo >= 0) {
				sb.append(value.substring(intFrom, intTo));
				sb.append(newString);
				intFrom = intTo + oldString.length();
			}
			else {
				sb.append(value.substring(intFrom));
				return sb.toString();
			}
		}
		while(true);
	}
	public static Set getDistinctValues(List<Map> list, String field){
		Set result = new HashSet();
		for(Map m:list){
			result.add( m.get(field));
		}
		return result;
	}
	public static String fillContentByVar(String jsonStr, Map<String, Object> env) {
		boolean isJson = jsonStr.trim().startsWith("{") || jsonStr.trim().startsWith("[");
	 
		for(String k:env.keySet())
		{
			if(jsonStr.indexOf("${" + k + "}")>=0){	
				Object envValue = env.get(k);
				if(envValue== null) {
					envValue = "";
				}
				if(envValue instanceof String) {
					//System.out.println("the k ->" + k);
					//System.out.println("the v ->" + env.get(k).toString());
					
				 

					jsonStr = CommonUtil.replace( jsonStr, "${" + k + "}",  isJson? YamlUtil.jsonEscape(envValue.toString()):envValue.toString());
				}
				else {
					 
					jsonStr = CommonUtil.replace( jsonStr,  "\"${" + k + "}\"",    isJson? YamlUtil.jsonEscape(envValue.toString()):envValue.toString());
					jsonStr = CommonUtil.replace( jsonStr, "${" + k + "}",  isJson? YamlUtil.jsonEscape(envValue.toString()):envValue.toString());
				}
				
			}
		}
		return jsonStr;
	}
	public static List<File> listSortedFiles(File file, final boolean isAsc) {
		File[] files = file.listFiles();
		List<File> result  = new ArrayList();
		for(File f:files) {
			result.add(f);
		}
		Collections.sort(result, new Comparator<File>() {

			@Override
			public int compare(File o1, File o2) {
				if(isAsc) {
					return o1.getName().compareTo(o2.getName());
				}
				else {
					return o2.getName().compareTo(o1.getName());
				}
				 
			}
			
		}
		);
		return result;
	}
	public static Date convertToCurrentTimeZone(String Date) {
        
        try {

            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = utcFormat.parse(Date);

            return date;
        }
        catch (Exception e){ e.printStackTrace();}

        return null;
	}
	public static Date convertUTC(String Date, String format) {
        
        try {

            DateFormat utcFormat = new SimpleDateFormat(format);
            //utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = utcFormat.parse(Date);
            long tt= date.getTime();
            return new Date(tt - TimeZone.getDefault().getOffset(tt));
            
        }
        catch (Exception e){ e.printStackTrace();}

        return null;
	}

	//get the current time zone
	
	public static String getCurrentTimeZone(){
	        TimeZone tz = Calendar.getInstance().getTimeZone();
	       
	        return tz.getID();
	}
	
	private static void printPixelARGB(int pixel) {
	    int alpha = (pixel >> 24) & 0xff;
	    int red = (pixel >> 16) & 0xff;
	    int green = (pixel >> 8) & 0xff;
	    int blue = (pixel) & 0xff;
	    System.out.println("argb: " + alpha + ", " + red + ", " + green + ", " + blue);
	  }
	 public static float[] convertGrayImgToFloat(  BufferedImage img)
	 {
		 

		 
		 
		 float[] result = new float[img.getWidth() * img.getHeight()];
		   // float [] bitfloat = null;
		   // bitfloat = img.getData().getPixels(0, 0, img.getWidth(), img.getHeight(), bitfloat);
		    int idx=0;
		    for(int y=0 ; y < img.getHeight(); y++)
		    {
		    	for(int x=0; x < img.getWidth(); x++)
		    	{
		    		 
		    		int i = img.getRGB(x, y);
		    		Color color = new Color(i);
		    		result[idx++] = 1- (float) ( 1.0 * color.getBlue()/255);
		    			
		    	}
		    	 
		    }

		 
		    return result;
	 }
	 
	 public static String getDataUrl(byte[] fileArray, String fileExtName) {
		 
	                    String imageString  = new String( Base64.getEncoder().encode(  fileArray ) );
	 
	                    return "data:image/"+ fileExtName +";base64, "+ imageString;
	                    
	 }
	 
	 public static String join(List<String>list, String delimeter) {
		 StringBuffer sb = new StringBuffer();
		 for(String s:list) {
			 if(sb.length()>0) {
				 sb.append(delimeter);
			 }
			 sb.append(s);
		 }
		 return sb.toString();
	 }
	 
	 public static int indexOf(String[] arr, String a) {
		 int idx= 0; 
		 for(String s:arr) {
			 if(s.equals(a)) {
				 return idx;
			 }
			 idx++;
		 }
		 return -1;
	 }
	 public static double[][][] convertColorImgToFloat(  BufferedImage img)
	 {
		 

		 
		 
		 double[][][] result = new double[img.getWidth()][img.getHeight()][4];
		   // float [] bitfloat = null;
		   // bitfloat = img.getData().getPixels(0, 0, img.getWidth(), img.getHeight(), bitfloat);
		    int idx=0;
		    for(int y=0 ; y < img.getHeight(); y++)
		    {
		    	for(int x=0; x < img.getWidth(); x++)
		    	{
		    		 
		    		int i = img.getRGB(x, y);
		    		Color color = new Color(i);
		    		result[x][y][0] =  (1.0 * color.getRed()/255);
		    		result[x][y][1] =  (1.0 * color.getGreen()/255);
		    		result[x][y][2] =  (1.0 * color.getBlue()/255);
		    		result[x][y][3] =  (1.0 * color.getAlpha()/255);		    			
		    	}
		    	 
		    }

		 
		    return result;
	 }
	 public static void readStream2Stream(InputStream from, OutputStream to ) throws IOException
	  {
	    byte[] buf = new byte[4096];
	    int len = 0;
	    while ((len = from.read(buf)) > 0)
	    {
	      to.write(buf, 0, len);
	    }
	  }
	 public static String readStream2String(InputStream from  , String encoding) throws IOException
	  {
	    byte[] buf = new byte[4096];
	    int len = 0;
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    while ((len = from.read(buf)) > 0)
	    {
	    	out.write(buf, 0, len);
	    }
	    return new String(out.toByteArray(),encoding);
	  }
	public static int getInt(Object o)
	{
		if(o==null)
		{
			return -1;
		}
		else
		{
			return Integer.parseInt(o.toString());
		}
	}
	public static String convertDateStr(Date date, String format)
	{
		SimpleDateFormat transFormat = new SimpleDateFormat(format);
		return transFormat.format(date);
	}
	public static Map<String, String> getQueryMap(String query)
	{
	    String[] params = query.split("&");
	    Map<String, String> map = new HashMap<String, String>();
	    for (String param : params)
	    {
			String[] paramArr = param.split("=");
	    	if(paramArr.length == 2) {
				String name = paramArr[0];
				String value = paramArr[1];
				map.put(name, value);
			}
	    }
	    return map;
	}
	public static long getSize(String o)
	{
		StringBuffer sb = new StringBuffer();
		int i=0;
		for(; i < o.length(); i++) {
			if((o.charAt(i)>='0' && o.charAt(i)<='9')  ) {
				continue;
			}
			else {
				break;
			}
		}
		return Long.parseLong(o.substring(0, i));
	}
	public static String getSizeUnit(String o)
	{
		int i=0;
		for(; i < o.length(); i++) {
			if((o.charAt(i)>='0' && o.charAt(i)<='9') || o.charAt(i) == '.') {
				continue;
			}
			else {
				break;
			}
		}
		return o.substring(  i);
	}
	public static void main(String[] args) {
		String size = "{\"E_list\":[],\"S_list\":[{\"DISK_UNIT\":\"G\",\"OLD_SPEC_INFO\":\"1vCore, 1GB RAM, 100GB Disk\",\"TEAM_CD\":\"1\",\"_SYSTEM_CODE_\":0,\"DC_ID\":\"17288018126300\",\"DISK_SIZE\":\"200\",\"GPU_SIZE\":\"0\",\"_USER_PGM_\":172,\"SPEC_ID\":\"0\",\"_COMP_ID_\":\"102216547406718\",\"CUD_CD\":\"U\",\"CPU_CNT\":\"1\",\"FROM_ID\":\"C1613172017084500\",\"_NOW_\":\"Tue Nov 17 10:54:31 KST 2020\",\"PRIVATE_IP\":\"10.150.3.2\",\"SVC_CD_NM\":\"단일서버\",\"OLD_RAM_SIZE\":\"1\",\"INS_DT\":\"20201117105421\",\"VM_NM\":\"GV001-12dev\",\"INS_ID\":\"12\",\"CATEGORY_NMS\":\"AI>음성인식>RNN\",\"_MILLIS_\":1605578071596,\"_ROLE_NM_\":\"ROLE_GRP_MEMBER\",\"_IS_SUPER_ADMIN_\":false,\"_TEAM_CD_\":\"1\",\"CUD_CD_NM\":\"변경\",\"_USER_ID_\":\"12\",\"_SYSDATE_\":\"20201117105431\",\"_CHILD_TEAM_CD_\":[\"1\"],\"OLD_DISK_SIZE\":\"100\",\"INS_PGM\":\"172\",\"INS_IP\":\"0:0:0:0:0:0:0:1\",\"OLD_DISK_UNIT\":\"G\",\"SVC_CD\":\"S\",\"_USER_IP_\":\"0:0:0:0:0:0:0:1\",\"VM_ID\":\"S1613834680649300\",\"_IS_ADMIN_\":\"N\",\"USE_MM\":\"10\",\"P_KUBUN\":\"dev\",\"OS_ID\":\"W2K16\",\"CATEGORY\":\"3\",\"_USER_NM_\":\"그룹 멤버\",\"_LANG_\":\"ko\",\"_SITE_CD_\":\"hynix\",\"PURPOSE\":\"web\",\"DISK_TYPE_ID\":\"1\",\"APPR_STATUS_CD_NM\":\"대기\",\"_LOGIN_ID_\":\"grp_mem@naver.com\",\"DISK_LIST\":[{\"DISK_ID\":\"D654631182419600\",\"DISK_SIZE\":\"100\",\"OLD_DISK_SIZE\":\"100\",\"KUBUN\":\"DISK\",\"DISK_TYPE_ID\":\"1\",\"DISK_NM\":\"/\"},{\"DISK_ID\":\"D480572169046200\",\"DISK_SIZE\":\"100\",\"KUBUN\":\"DISK_REQ\",\"DISK_TYPE_ID\":\"1\",\"CUD_CD\":\"C\",\"DISK_NM\":\"/data2\"}],\"OLD_CPU_CNT\":\"1\",\"APPR_STATUS_CD\":\"R\",\"LAST_INS_DT\":\"20200821134809\",\"OLD_GPU_SIZE\":\"0\",\"RAM_SIZE\":\"1\",\"SPEC_INFO\":\"1vCore, 1GB RAM, 200GB Disk\"}],\"C_list\":[],\"V_list\":[]}";
		JSONObject o = new JSONObject(size);
		Object r = CommonUtil.convertJSONToMapOrList(o);
		
		System.out.println(r);
		 
		
		
	}
	public static void chkName(String val, boolean firstAlpha) throws Exception {
		if(val==null || "".equals(val)) {
			throw new Exception("Empty value");
		}
		if(val.length()>63) {
			throw new Exception("63자 이내만 사용하실 수 있습니다.");
		}
		String pattern = "[a-z0-9]([-a-Z0-9]*[a-z0-9])?";
		if(firstAlpha) {
			pattern = "[a-Z]([-a-Z0-9]*[a-z0-9])?";
		}
		if(!val.matches(pattern)) {
			if(firstAlpha) {
				throw new Exception("영문 소문자로 시작하고 끝나야 하며 영문소문자, -, 숫자만 사용할 수 있습니다.");
			}
			else {
				throw new Exception("영문 소문자, 숫자로 시작하고 끝나야 하며 영문소문자, -, 숫자만 사용할 수 있습니다.");
			}
		}
	}
	public static void chkRequried(Map param, String... fields) throws Exception {
		for(String f:fields) {
			if(  param.get(f) == null || "".equals(param.get(f) ) ) {
				throw new Exception(f + "' value is null");
			}
		}
	}
	 
	public static List<Map> convertList(JSONArray arr, String... field) {
		List result = new ArrayList();
		for(int i=0; i < arr.length(); i++) {
			result.add(convertJSONToMapOrList(arr.get(i) , field));
		}
		return result;
	}
	
	public static void putAll(JSONObject target, JSONObject src) {
		Iterator it = src.keys();
		while(it.hasNext()) {
			String key = (String)it.next();
			target.put(key, src.get(key));
		}
	}
	public static Object convertJSONToMapOrList(Object o, String... field)  {
		if(o instanceof JSONObject) {
			return converMap( (JSONObject) o, field);
		}
		if(o instanceof JSONArray) {
			return convertList((JSONArray) o, field);
		}
		return o;
	}
	public static Map converMap(JSONObject map, String... field) {
		Map result = new HashMap();
		if(field==null || field.length==0) {
			for(Object f:map.keySet()) {
				result.put(f, convertJSONToMapOrList(map.get((String)f)));
			}
		}
		else {
			for(String f:field) {
				result.put(f, convertJSONToMapOrList(map.get(f)));
			}
		}
		
		return result;
	}
	public static String getInnerStr(String str, String start, String end) {
		int pos = str.indexOf(start);
		if(pos>=0) {
			int pos1 = str.indexOf( end,pos+start.length()+1);
			if(pos1>pos) {
				return str.substring(pos+start.length() , pos1);
			}
		}
		return null;
	}
	public static String getReplaceInnerStr(String str, String start, String end, String replaceStr) {
		int pos = str.indexOf(start);
		if(pos>=0) {
			int pos1 = str.indexOf( end,pos+start.length()+1);
			if(pos1>pos) {
				return str.substring(0,pos+start.length()+1) + replaceStr + str.substring(pos1);
			}
		}
		return null;
	}
	
	public static Object nvl(Object obj, Object def) {
		if(obj == null) {
			return def;
		}
		else {
			return obj;
		}
	}
}
