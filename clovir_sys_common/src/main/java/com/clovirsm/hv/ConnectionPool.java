package com.clovirsm.hv;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

 

public class ConnectionPool  {
	private static ConnectionPool me1;
	private Map<String, ObjectPool<IConnection>> pools;

	public static synchronized ConnectionPool getInstance() throws Exception {
		 if(me1==null) me1 = new ConnectionPool();
		 return me1;
	}
	class ConnectionFactory extends BasePooledObjectFactory<IConnection>
	{
		String url;
		String id;
		String pwd;
		String className;
		Map prop ;
		public ConnectionFactory(String className, String url, String id, String pwd, Map prop)
		{
			this.url = url;
			this.id = id;
			this.pwd = pwd;
			this.className = className;
			this.prop = prop;
		}

		@Override
		public boolean validateObject(PooledObject<IConnection> p) {

			if( super.validateObject(p))
			{
				return p.getObject().isConnected();
			}
			return false;
		}

		@Override
		public IConnection create() throws Exception {
			 
			if(className == null) return null;
			IConnection mng = (IConnection)Class.forName(className).newInstance();
			mng.connect(url, id, pwd, prop );
			return mng;
		}
		@Override
		public PooledObject<IConnection> wrap(IConnection mng) {
			 return new DefaultPooledObject<IConnection>(mng);
		}
	}
	private ConnectionPool() throws Exception
	{
		RestClient.trustAllHttpsCertificates();
		init();
	}
	private void init() {

		pools = new HashMap();
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@Connection Pool init @@@@@@@@@@@@@@@@@@@@@@@@");


	}
	private String getKey(IConnection mng)
	{
		return getKey(mng.getClass().getName(), mng.getURL());
	}
	private String getKey(String type, String url)
	{
		return type + url;
	}
	public boolean release(IConnection mng)
	{
		ObjectPool<IConnection> pool = pools.get(getKey(mng));
		if(pool != null) {
			try {
				pool.returnObject(mng);
				return true;
			} catch (Exception e) {
			 
				e.printStackTrace();
			}
			
		}  
		
		return false;
		
	}

	public IConnection getNewConnection(String type, String url, String id, String pwd, Map prop) throws Exception
	{
		ConnectionFactory f = new ConnectionFactory(type, url, id, pwd, prop);
		return f.create();
	}
	public IConnection getConnection(String className, String url, String id, String pwd, Map prop) throws Exception{
		return  getConnection(  className,   url,   id,   pwd,   prop, 5, 0);
	}
	public void clearPool(String className, String url) throws Exception{
		ObjectPool<IConnection> pool = pools.remove(getKey(className, url));
		if(pool != null) {
			pool.clear();
		}
	}
	public void printCurrentStatus(Writer writer) throws Exception{
		for(String key:pools.keySet()) {
			writer.append(key + ": active=" + pools.get(key).getNumActive() + ", idle=" + pools.get(key).getNumIdle() + "\n");
		}
	}
	public synchronized IConnection getConnection(String className, String url, String id, String pwd, Map prop, int maxTotal, int initialSize) throws Exception
	{
		//System.out.println("getConnection" + className + ":" + pools.keySet() + "==" + getKey(className, url));
		ObjectPool<IConnection> pool = pools.get(getKey(className, url));
		if(pool== null)
		{
			ConnectionFactory f = new ConnectionFactory(className, url, id, pwd, prop);
			pool = new GenericObjectPool<IConnection>(f);

			GenericObjectPoolConfig conf = new GenericObjectPoolConfig();
			conf.setBlockWhenExhausted(false);
			conf.setMaxTotal(maxTotal);
			conf.setTestOnBorrow(true);
			((GenericObjectPool)pool).setConfig(conf );
			 
			for (int i = 0 ; i < initialSize  ; i++) {
					pool.addObject();
			}
			pools.put(getKey(className, url), pool);
		}
		while(true)
		{
			try
			{
				return pool.borrowObject();
			}
			catch(NoSuchElementException e)
			{

			}
		}





	}


}
