package com.clovirsm.hv;

import java.util.Map;
 

public interface IAPI {
	public IConnection connect(Map param , int maxSize, int initialSize) throws Exception;
	public IConnection connect(Map param  ) throws Exception;
	//public void disconnect(IConnection conn);
}
