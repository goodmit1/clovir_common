package com.clovirsm.hv;

import java.util.Map;
/**
 * Hypervisor Async로 실행 후 사후 작업 정의
 * @author 윤경
 *
 */
public interface IAfterProcess {
	/**
	 * Task 성공 하였을 때 실행
	 * @param taskId
	 * @throws Exception
	 */
	public void onAfterSuccess( String taskId ) throws Exception ;
	
	/**
	 * 생성 전 parameter
	 * @return
	 */
	Map getParam(); 
	
	public void chkState( ) ;
	
	public void setTask(String taskId);
	public void startTask(String taskId);
	
	/**
	 * Task 실패 하였을 때 실행
	 * @param taskId
	 * @param e
	 * @throws Exception
	 */
	void onAfterFail(String taskId, Throwable e) throws Exception;

}
