package com.clovirsm.hv;

import java.util.Map;
 
public interface IConnection {
	 
	 public String getURL();
	 public void connect(String url, String userId, String pwd, Map prop) throws Exception;
	 public void disconnect();
	 public boolean isConnected();
	 
	 
}
