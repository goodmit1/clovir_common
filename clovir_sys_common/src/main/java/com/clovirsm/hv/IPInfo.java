package com.clovirsm.hv;

public class IPInfo
{
	public int nicId;
	public String ip;
	public String nw_ip;
	public String gateway;
	public String subnetMask;
	public String network;
	public String macAddress;
}