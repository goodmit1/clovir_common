package com.clovirsm.hv;

public class NotFoundException extends  Exception{
	public NotFoundException(String... args)
	{
		super("Not Found==>" +  args[0] + (args.length>1 ? ":" + args[1]:""));
	}
}
