package com.clovirsm.hv;
 

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

public class NumberUtil {

	private static String removeComma(String s)
	{
		return s.replaceAll(",", "");
	}
	public static String format(long num) {
		 NumberFormat nf3 = NumberFormat.getInstance(new Locale("ko", "KR"));
		 return nf3.format(num);
	}
	public static String format(int num) {
		 NumberFormat nf3 = NumberFormat.getInstance(new Locale("ko", "KR"));
		 return nf3.format(num);
	}
	public static Long getLong(Object o)
	{
		if(o == null) return null;
		if(o instanceof BigDecimal)
		{
			return ((BigDecimal)o).longValue();
		}
		else if(o instanceof Double)
		{
			return ((Double)o).longValue();
		}
		else if(o instanceof Integer)
		{
			return ((Integer)o).longValue();
		}
		else if(o instanceof String)
		{
			try
			{
				String s = removeComma((String)o);
				return Long.parseLong(s);
			}
			catch(Exception e)
			{
				return null;
			}
		}
		else 
		{
			return (Long)o;
		}
	}
	public static Float getFloat(Object o)
	{
		if(o == null) return null;
		if(o instanceof BigDecimal)
		{
			return ((BigDecimal)o).floatValue();
		}
		else if(o instanceof Long)
		{
			return ((Long)o).floatValue();
		}
		else if(o instanceof String)
		{
			try
			{
				String s = removeComma((String)o);
				return Float.parseFloat(s);
			}
			catch(Exception e)
			{
				return null;
			}
		}
		else if(o instanceof Double)
		{
			try
			{
				return ((Double)o).floatValue();
			}
			catch(Exception e)
			{
				return null;
			}
		}
		else 
		{
			return (Float)o;
		}
	}
	public static long getLong(Object o, long def)
	{
		
		Long r =  getLong(o);
		if(r == null) return def;
		return r;
	}
	public static int getInt(Object o, int def)
	{
		
		Integer r =  getInt(o);
		if(r == null) return def;
		return r;
	}
	
	public static boolean getBoolean(Object o, boolean def)
	{
		if(o == null)
		{
			return def;
		}
		if(o instanceof Boolean)
		{
			return (boolean)o;
		}
		else if(o instanceof String)
		{
			return "true".equals(o);
		}
		return def;
	}
	public static Integer getInt(Object o)
	{
		 
		if(o == null) return null;
		if(o instanceof BigDecimal)
		{
			return ((BigDecimal)o).intValue();
		}
		else if(o instanceof Double)
		{
			return ((Double)o).intValue();
		}
		else if(o instanceof Long)
		{
			return ((Long)o).intValue();
		}
		else if(o instanceof String)
		{
			try
			{
				String s = removeComma((String)o);
				return Integer.parseInt(s);
			}
			catch(Exception e)
			{
				return null;
			}
		}
		else 
		{
			return (Integer)o;
		}
	}
	
	public static Double getDouble(Object o)
	{
		 
		if(o == null) return null;
		if(o instanceof BigDecimal)
		{
			return ((BigDecimal)o).doubleValue();
		}
		else if(o instanceof Integer)
		{
			return ((Integer)o).doubleValue();
		}
		else if(o instanceof Long)
		{
			return ((Long)o).doubleValue();
		}
		else if(o instanceof String)
		{
			try
			{
				String s = removeComma((String)o);
				return Double.parseDouble(s);
			}
			catch(Exception e)
			{
				return null;
			}
		}
		else 
		{
			return (Double)o;
		}
	}
}
