package com.clovirsm.hv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.XML;

public class ParamObj {

	String name;
	Map<String, Object> attribute;
	List children;
	public ParamObj(String name)
	{
		this(name, (Map)null);
	}
	public ParamObj(String name, String content)
	{
		this(name, (Map)null);
		this.addChild(content);
	}
	public ParamObj(String name,Map attr)
	{
		this.name = name;
		this.attribute = new HashMap();
		
		children = new ArrayList();
		if(attr != null)
		{
			this.addAttr(attr);
		}
			
	}
	public String toXML()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("<" + name );
		for(String a:attribute.keySet())
		{
			sb.append(" " + a + "=\"" + attribute.get(a) + "\"");
		}
		sb.append(">\n");
		for(int i=0; i < children.size(); i++)
		{
			Object o = children.get(i);
			if(o instanceof ParamObj)
			{
				sb.append(((ParamObj)o).toXML());
			}
			else if(o instanceof List )
			{
				List<ParamObj> list = (List<ParamObj>)o;
				for(ParamObj o1 : list)
				{
					sb.append(((ParamObj)o1).toXML());
				}
			}
			else if(o instanceof String)
			{
				sb.append(XML.escape((String)o));
			}
		}
		sb.append("</" + name  + ">\n");
		return sb.toString();
	}
	public void addAttr(String name, String val)
	{
		this.attribute.put(name, val);
		
	}
	public void addAttr(Map attr)
	{
		this.attribute.putAll(attr);
	}
	public void addChild(ParamObj obj)
	{
		this.children.add(obj);
	}
	public void addChild(Map<String, Object> map)
	{
		for(String k : map.keySet())
		{
			ParamObj obj = new ParamObj(k);
			obj.addChild(map.get(k).toString());
			this.children.add(obj);	
		}
		
	}
	public void addChild(String obj)
	{
		this.children.add(obj);
	}
	public void addChild(List<ParamObj> obj)
	{
		this.children.add(obj);
	}
}
