package com.clovirsm.hv;


import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSubsystem;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Logger;
import com.jcraft.jsch.Session;

/**
 * Helper class for building {@link Session} objects.
 *
 * @author vladimir.stankovic (svlada@gmail.com)
 *
 */
public class PublicKeySshSession {

    final Session session;

    public PublicKeySshSession(final Builder builder) {
        this.session = builder.jschSession;
    }

    public void disconnect() throws JSchException {

        session.disconnect();


    }
    public String execute(String command, boolean isPowerShell) throws  Exception {
        Channel channel = null;
        if (command != null && command.isEmpty()) {
            throw new IllegalArgumentException("SSH command is blank.");
        }
        try{
            channel = session.openChannel( "exec");
            String cmd = (isPowerShell ? "powershell  -command \"Start-Process powershell -Verb runAs -Args '" + command + "'\"":  command);
            System.out.println(cmd);
            ((ChannelExec) channel).setCommand( cmd );

            ((ChannelExec) channel).setPty(false);
            channel.setInputStream(null);
            channel.connect();



            channel.setOutputStream(System.out);
            ByteArrayOutputStream err = new ByteArrayOutputStream();;
            channel.setExtOutputStream(err);


            int BUFFER_SIZE = 1024;
            InputStream inputStream = channel.getInputStream();

            byte[] buffer = new byte[BUFFER_SIZE];


            StringBuffer output = new StringBuffer();
            while(true) {
                while (inputStream.available() > 0) {
                    int i = inputStream.read(buffer, 0, BUFFER_SIZE);
                    if (i < 0) {
                        break;
                    }
                    output.append(new String(buffer, 0, i));

                }


                if (channel.isClosed()) {
                    if (inputStream.available() > 0  ) {
                        continue;
                    }
                    break;
                }

                Thread.sleep(1000);
            }

            final int exitStatus = channel.getExitStatus();
            System.out.println("Exit Status : " + exitStatus);
            if(exitStatus != 0){

                throw new Exception (err.toString());
            }


            channel.disconnect();
            return output.toString();


        } catch (JSchException e) {
            e.printStackTrace();
            throw new RuntimeException("Error durring SSH command execution. Command: " + command);
        }
    }

    public static class Builder {
        private String host;
        private String username;
        private String password;
        private int port;
        private Path privateKeyPath;
        private byte[] prvKey;

        private Session jschSession;

        public Builder(String host, String username, int port,  String password,  String path) {
            this.host =  host;
            this.username = username;
            this.port = port;
            if(path != null) this.privateKeyPath = Paths.get(path);
            this.password = password;
        }
        public static byte[] convertPrvKey(String key) {
            return key.getBytes();
        }
        public Builder(String host, String username, int port, byte[] prvKey) {
            this.host =  host;
            this.username = username;
            this.port = port;
            this.prvKey = prvKey;
        }
        private void validate() {
            if (port < 1) {
                throw new IllegalArgumentException("Port number must start with 1.");
            }
        }

        public PublicKeySshSession build() {
            validate();



            JSch jsch = new JSch();

            Session session = null;

            try {
                if(privateKeyPath != null) {
                    jsch.addIdentity(privateKeyPath.toString());
                }
                else if(prvKey != null){
                    jsch.addIdentity(this.username, prvKey, null, null);

                }

                session = jsch.getSession(username, host, port);
                if(password != null) {
                    session.setPassword(password);
                }
                session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");

                java.util.Properties config = new java.util.Properties();
                config.put("StrictHostKeyChecking", "no");

                session.setConfig(config);
                if (session == null) {
                    throw new IllegalArgumentException("Session object is null.");
                }
                session.connect();
            } catch (JSchException e) {
                throw new RuntimeException("Failed to create Jsch Session object.", e);
            }

            this.jschSession = session;

            return new PublicKeySshSession(this);
        }



    }

}