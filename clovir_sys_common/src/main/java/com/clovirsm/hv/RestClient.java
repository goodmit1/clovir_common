package com.clovirsm.hv;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
 




public class RestClient {
	
	public static void main(String[] args)
	{
		try
		{
			ParamObj obj = new ParamObj("section");
			
			obj.addAttr("name", "syk");
			
			/*ParamObj rule = new ParamObj("rule");
			obj.addChild(rule);
			rule.addAttr("id","2");
			rule.addAttr("disabled","false");
			rule.addAttr("logged","false");
			rule.addChild(new ParamObj("name","Default Rule"));
			rule.addChild(new ParamObj("action","DENY"));
			ParamObj appliedToList = new ParamObj("appliedToList");
			ParamObj appliedTo = new ParamObj("appliedTo");
			appliedToList.addChild(appliedTo);
			appliedTo.addChild(new ParamObj("name","DISTRIBUTED_FIREWALL"));
			appliedTo.addChild(new ParamObj("value","DISTRIBUTED_FIREWALL"));
			appliedTo.addChild(new ParamObj("type","DISTRIBUTED_FIREWALL"));
			appliedTo.addChild(new ParamObj("isValid","true"));
			rule.addChild(appliedToList);
			*/
			 
			
		 
			//https://172.16.18.80
			RestClient client = new RestClient("https://172.16.18.83","administrator@vsphere.local","VMware1!");
			//System.out.println(client.get("/api/4.0/edges"));
			//System.out.println(client.get("/api/4.0/firewall/globalroot-0/config"));
			
			 
			System.out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+obj.toXML());
			System.out.println(client.post("/api/4.0/firewall/globalroot-0/config/layer3sections", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+obj.toXML()));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	String baseUrl;
	

	String authStr;
	String token;
	
	String mimeType = "application/xml";
	String dateFormat ;
	Map<String,String> header;
	
	public String getBaseUrl() {
		return baseUrl;
	}
	public String getToken() {
		return token;
	}

	public void setMimeType(String mimeType)
	{
		this.mimeType = mimeType;
	}
	public Map getHeader()
	{
		return this.header;
	}
	public void setHeader(Map header)
	{
		this.header = header;
	}
	public void setToken(String token)
	{
		this.token = token;
	}
	
	static
	{
		try
		{
			trustAllHttpsCertificates();
		}
		catch (Exception localException) {
		}
	}
	public RestClient(String url)
	{
		setBaseUrl( url);
	}
	public void setBaseUrl(String url)
	{
		this.baseUrl = url;
	}
	public RestClient(String url, String userId, String pwd) throws Exception
	{
		authStr = Base64.getEncoder().encodeToString( (userId+":" + pwd).getBytes() );
		this.baseUrl = url;
		
	}
	public void setAuthStr(String str) {
		authStr = str ;
	}
	public String getAuthStr() {
		return authStr;
	}
	private static class TrustAllManager
	implements X509TrustManager
	{


		public X509Certificate[] getAcceptedIssuers()
		{
			return null;
		}

		public void checkServerTrusted(X509Certificate[] certs, String authType)
				throws CertificateException
		{
		}

		public void checkClientTrusted(X509Certificate[] certs, String authType)
				throws CertificateException
		{
		}
	}
	public static SSLContext trustAllHttpsCertificates()
			throws NoSuchAlgorithmException, KeyManagementException
	{
		TrustManager[] trustAllCerts = new TrustManager[1];
		trustAllCerts[0] = new TrustAllManager();
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		HttpsURLConnection.setDefaultSSLSocketFactory(
				sc.getSocketFactory());
	 
		    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier()
		        {
		            public boolean verify(String hostname, SSLSession session)
		            {
		               
		                    return true;
		               
		            }
		        });
		    return sc;
		 
	}
	/*
	private void setLogin(String username, String password)
	{
		final String user = username;
		final String pass = password;
		Authenticator.setDefault(new Authenticator()
		{
			protected PasswordAuthentication getPasswordAuthentication()
			{
				return new PasswordAuthentication(user, pass.toCharArray());
			}
		});
	}*/
	static String urlEncodeUTF8(String s) {
		try {
			return URLEncoder.encode(s, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new UnsupportedOperationException(e);
		}
	}
	public static String urlEncodeUTF8(Map<?,?> map) {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<?,?> entry : map.entrySet()) {
			if (sb.length() > 0) {
				sb.append("&");
			}
			sb.append(String.format("%s=%s",
					urlEncodeUTF8(entry.getKey().toString()),
					urlEncodeUTF8(entry.getValue().toString())
					));
		}
		return sb.toString();       
	}
	protected void setAuth(	HttpURLConnection conn )
	{
		if(token != null) conn.setRequestProperty("Authorization", "Bearer " + token);
		else if(authStr != null) conn.setRequestProperty("Authorization", "Basic " + authStr);
		if(header != null)
		{
			for(String h: header.keySet())
			{
				conn.setRequestProperty(h, header.get(h));
			}
		}
	}
	public Object postFiles(String path, Map<String, Object> params, File[] files) throws Exception{
		String boundary = "^-----^";
        String LINE_FEED = "\r\n";
        String charset = "UTF-8";
        OutputStream outputStream;
        PrintWriter writer;

       
        

            URL url = new URL(baseUrl + path);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            setAuth(connection);
            connection.setRequestProperty("Content-Type", "multipart/form-data;charset=utf-8;boundary=" + boundary);
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(5000);

            outputStream = connection.getOutputStream();
            writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);

            /** Body에 데이터를 넣어줘야 할경우 없으면 Pass **/
            for(String key:params.keySet()) {
	            writer.append("--" + boundary).append(LINE_FEED);
	            writer.append("Content-Disposition: form-data; name=\"" + key + "\"").append(LINE_FEED);
	            writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
	            writer.append(LINE_FEED);
	            writer.append(params.get(key).toString()).append(LINE_FEED);
	            writer.flush();
            }
            for(File file: files) {
	            /** 파일 데이터를 넣는 부분**/
	            writer.append("--" + boundary).append(LINE_FEED);
	            writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + file.getName() + "\"").append(LINE_FEED);
	            writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(file.getName())).append(LINE_FEED);
	            writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
	            writer.append(LINE_FEED);
	            writer.flush();
	
	            FileInputStream inputStream = new FileInputStream(file);
	            byte[] buffer = new byte[(int)file.length()];
	            int bytesRead = -1;
	            while ((bytesRead = inputStream.read(buffer)) != -1) {
	                outputStream.write(buffer, 0, bytesRead);
	            }
	            outputStream.flush();
	            inputStream.close();
	            writer.append(LINE_FEED);
	            writer.flush();
	
	            writer.append("--" + boundary + "--").append(LINE_FEED);
            }
            writer.close();

            try
    		{
            	System.out.println(connection.getResponseCode());
            	//System.out.println(connection.getHeaderFields());
    			return readStream(connection.getHeaderField("Content-Type"), connection.getInputStream());
    		}
    		catch(Exception e)
    		{
    			InputStream stream = connection.getErrorStream();
    	    	if(stream != null)
    	    	{
    		    	ByteArrayOutputStream out1 = new ByteArrayOutputStream();
    				CommonUtil.readStream2Stream(stream, out1);
    				 
    				throw new IOException( new String(out1.toByteArray()) );
    	    	}
    	    	throw e;
    			
    		}
         
    }

	boolean redirect=true;
	public int getResponseCode() {
		return responseCode;
	}
	public Map<String, List<String>> getResponseHeaders() {
		return responseHeaders;
	}

	int responseCode;
	Map<String, List<String>> responseHeaders;
	public boolean isRedirect() {
		return redirect;
	}
	public void setRedirect(boolean redirect) {
		this.redirect = redirect;
	}
	public Object send(String method, String path,String param) throws  Exception
	{
		HttpURLConnection conn = null;
		try {
		 
			conn = (HttpURLConnection)new URL(baseUrl + path).openConnection();
			
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(5000);
			conn.setRequestMethod(method);
			setAuth(conn);
			 
			  
			conn.setUseCaches(false);
	
	
			String body = null; 
			if(param != null)
			{
				body = param;
			}
		 
			conn.setDoInput(true);
			conn.setInstanceFollowRedirects(redirect);
			if(body != null)
			{	
	
				conn.setRequestProperty("Content-Type", mimeType + ";chatset=utf-8");
				conn.setRequestProperty( "Content-Length", "" + body.length());
				conn.setDoOutput(true);
				 
				OutputStream os = conn.getOutputStream();
				 
				os.write(body.getBytes("utf-8"));
	
				 
				os.flush();
	
				 
				os.close();
			}
		 
			responseCode = conn.getResponseCode();
			responseHeaders = conn.getHeaderFields();
			return readStream(conn.getHeaderField("Content-Type"), conn.getInputStream());
		}
		catch(Exception e)
		{
			System.out.println(this.baseUrl + path);
			if(conn != null) {
				InputStream stream = conn.getErrorStream();
		    	if(stream != null)
		    	{
			    	ByteArrayOutputStream out1 = new ByteArrayOutputStream();
					CommonUtil.readStream2Stream(stream, out1);

					throw new IOException(  new String(out1.toByteArray()) );
		    	}
			}
	    	throw e;
			
		}
	}
	public Date parseDate(String date) throws ParseException
	{
		 SimpleDateFormat parser = new SimpleDateFormat();
		 if(dateFormat != null) parser.applyPattern(dateFormat);
		 return parser.parse(date);
	}
	public JSONObject getJSON(String path) throws  Exception
	{
		return (JSONObject)send("GET",path, null);
	}
	public  Object get(String path) throws  Exception
	{
		return  send("GET",path, null);
	}
	public  Object get(String path, String param) throws  Exception
	{
		return  send("GET",path, param);
	}
 
	public Object post(String path, String param) throws  Exception
	{
		return send("POST",path, param);
	}
	public Object put(String path, String param) throws  Exception
	{
		return send("PUT",path, param);
	}
	public Object patch(String path, String param) throws  Exception
	{
		return send("PATCH",path, param);
	}
	public Object delete(String path) throws  Exception
	{
		return send("DELETE",path, null);
	}
	public Object delete(String path, String param) throws  Exception
	{
		return send("DELETE",path, param);
	}
 
	private Object readStream(String type, InputStream is) throws  Exception
	{
		ByteArrayOutputStream to = new ByteArrayOutputStream();
		try
		{
			byte[] buf = new byte[4096];
			int len = 0;
			while ((len = is.read(buf)) > 0)
			{
				to.write(buf, 0, len);
			}
			
			if(type != null && type.startsWith("text"))
			{
				 
				String str = to.toString("utf-8");
				to.close();
				return str;
			}
			else if(type != null && type.startsWith("application/xml"))
			{
			
				String str = to.toString("utf-8");
				to.close();
				return XML.toJSONObject(str);
			}
			else if(type != null && type.startsWith("application/json"))
			{
				String str = to.toString("utf-8");
				to.close();
				str = str.trim();
				try {
					if(str.startsWith("{")) {
						return new  JSONObject(str);
					}
					else {
						return new  JSONArray(str);	
					}
				}
				catch(Exception e) {
					return str;
				}
			}
				 
			else
			{
				return to.toByteArray() ;
			}
			 
		}
		finally
		{
			is.close();
		}
	}
	public String getDateFormat() {
		return dateFormat;
	}
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
}
