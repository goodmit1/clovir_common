package com.clovirsm.hv;
 
import java.util.Map;
import java.util.Set;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

public class SessionRestClient {
	String baseUrl;
	CloseableHttpClient httpClient;
	String mimeType = "application/xml";
	String token = null;
	String authStr = null;
	Map header = null;
	public void setMimeType(String mimeType)
	{
		this.mimeType = mimeType;
	}
	public void setToken(String token)
	{
		this.token = token;
	}
	public void setHeader(Map header)
	{
		this.header = header;
	}
	public SessionRestClient(String baseUrl) throws Exception{
		this.baseUrl = baseUrl;
		SSLContext sslcontext  = RestClient.trustAllHttpsCertificates();
		
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			httpClient = HttpClients.custom()
			    .setSSLSocketFactory(sslsf)
			    .build();
	}
	protected void setAuth(	HttpRequestBase base )
	{
		if(token != null) base.setHeader("Authorization", "Bearer " + token);
		else if(authStr != null) base.setHeader("Authorization", "Basic " + authStr);
	}
	protected Object send(HttpRequestBase base) throws Exception{
		base.setHeader("Content-Type", mimeType + ";chatset=utf-8");
		if(header != null)
		{
			Set<String> keys = header.keySet();
			for(String k : keys)
			{
				base.setHeader(k, (String)header.get(k));
			}
		}
		HttpResponse response = httpClient.execute(base);
		if(response.getEntity()==null) {
			return null;
		}
		String body =  EntityUtils.toString(response.getEntity()).trim();
		System.out.println(response.getStatusLine() + ":" +body);
		if("application/xml".equals(mimeType)) {
			return XML.toJSONObject(body);
		}
		else if("application/json".equals(mimeType)) {
		 
			if(body.startsWith("{")) {
				return new  JSONObject(body);
			}
			else {
				return new  JSONArray(body);	
			}
		}
		else {
			return body;
		}
		 
	}
	
	public JSONObject getJSON(String path) throws  Exception
	{
		HttpGet get = new HttpGet(this.baseUrl + path);
		return (JSONObject)send(get);
	}
	public  Object get(String path) throws  Exception
	{
		HttpGet get = new HttpGet(this.baseUrl + path);
		return  send(get);
	}
	 
 
	public Object post(String path, String param) throws  Exception
	{
		HttpPost post = new HttpPost(this.baseUrl + path);
		StringEntity se = new StringEntity(param);
		post.setEntity(se);
		return  send(post);
	}
	public Object put(String path, String param) throws  Exception
	{
		HttpPut post = new HttpPut(this.baseUrl + path);
		StringEntity se = new StringEntity(param);
		post.setEntity(se);
		return  send(post);
	}
	public Object delete(String path) throws  Exception
	{
		HttpDelete delete = new HttpDelete(this.baseUrl + path);
		 
		return  send(delete);
	}
}
