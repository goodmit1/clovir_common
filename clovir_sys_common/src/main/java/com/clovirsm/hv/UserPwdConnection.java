package com.clovirsm.hv;

import java.util.Date;
import java.util.Map;

public class UserPwdConnection  implements IConnection{
	String url;
	String userId;
	String pwd;
	 
	RestClient client;
	Date expire;
	String token;
	boolean connect = false;
	public  RestClient getRestClient()
	{
		return client;
	}
	 

	@Override
	public String getURL() {
		return url;
	}

 
	@Override
	public void connect(String url, String userId, String pwd, Map prop) throws Exception {
		this.url = url;
		 
		client = new RestClient(url, userId, pwd);
		 
		expire = new Date();
		expire.setMinutes(expire.getMinutes() + 10);
		
		 
		connect = true;
	}

	 

	@Override
	public boolean isConnected() {
		return client == null || expire.compareTo(new Date())>0;
		
	}

	 
	@Override
	public void disconnect() {
		client = null;
		
	}

}
