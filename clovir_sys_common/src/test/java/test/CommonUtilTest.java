package test;

import com.clovirsm.hv.CommonUtil;
import org.junit.Assert;
import org.junit.Test;

public class CommonUtilTest {
    @Test
    public void getQueryMap(){
        java.util.Map m = CommonUtil.getQueryMap("a=1&b=2");
        Assert.assertEquals(m.get("a"),"1");
        m = CommonUtil.getQueryMap("a=&b=");
        Assert.assertEquals(m.get("a"),null);
    }
}
